# -*- coding: utf-8 -*-
# Copyright 2016 OpenSynergy Indonesia
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).
# Modified by Arkana Solusi Digital 2018

from . import ptkp
from . import pph_21_biaya_jabatan
from . import pph_21_rate
from . import pph_21_npwp_rate_modifier
from . import res_partner