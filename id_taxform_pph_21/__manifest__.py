# -*- coding: utf-8 -*-
# Copyright 2016 OpenSynergy Indonesia
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).
# Modified by Arkana Solusi Digital 2018
{
    "name": "Indonesia's PPh 21 Taxform Related Configuration and Computation",
    "version": "11.0",
    "category": "localization",
    "website": "https://www.arkana.co.id/",
    "author": "OpenSynergy Indonesia,"
              "Odoo Community Association (OCA),"
              "Arkana Solusi Digital,"
              "Joenan,"
              "Ardian",
    "license": "AGPL-3",
    "installable": True,
    'depends': [
        'id_taxform',
        'account',
        'efaktur',
    ],
    'data': [
        'security/ir.model.access.csv',
        'data/ptkp_category_data.xml',
        'views/res_partner_views.xml',
        'views/ptkp_views.xml',
        'views/pph_21_rate_views.xml',
        'views/pph_21_npwp_rate_modifier_views.xml',
        'views/pph_21_biaya_jabatan_views.xml',
    ],
}