# -*- coding: utf-8 -*-
# Copyright 2016 OpenSynergy Indonesia
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).
# Modified by Arkana Solusi Digital 2018
{
    "name": "Indonesia's Taxform - Tax Period",
    "version": "11.0.0.0.0",
    "category": "Hidden",
    "website": "https://www.keepapp.co.id/",
    "author": "OpenSynergy Indonesia,"
              "Odoo Community Association (OCA),"
              "Arkana Solusi Digital,"
              "Joenan,"
              "Ardian",
    "license": "AGPL-3",
    "application": False,
    "installable": True,
    'depends': [
        'id_taxform'
    ],
    'data': [
        'security/ir.model.access.csv',
        'views/tax_period_views.xml',
    ],
    'demo': [
        'demo/tax_period_demo.xml',
    ],
}