# -*- coding: utf-8 -*-
# Copyright 2016 OpenSynergy Indonesia
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).
# Modified by Arkana Solusi Digital 2018
from odoo import models, fields, api, _
from odoo.exceptions import ValidationError

from datetime import datetime
from dateutil.relativedelta import relativedelta


class TaxPeriod(models.Model):
    _name = 'tax.period'
    _description = "Tax period"
    _order = "date_start asc, id"


    name = fields.Char('Tax Year', required=True)
    code = fields.Char('Code', required=True)
    date_start = fields.Date('Date Start', required=True)
    date_end = fields.Date('Date End', required=True)
    year_id = fields.Many2one('tax.year', 'Tax Year', ondelete="cascade",)

    @api.constrains("date_start", "date_end")
    def _check_range(self):
        if self.date_end <= self.date_start:
            strWarning = _("The start date must precede it's end date")
            raise models.ValidationError(strWarning)

    @api.model
    def _find_period(self, dt=None):
        if not dt:
            dt = datetime.now().strftime("%Y-%m-%d")
        criteria = [
            ("date_start", "<=", dt),
            ("date_end", ">=", dt),
        ]
        results = self.search(criteria)
        if not results:
            strWarning = _("No tax period configured for %s" % dt)
            raise models.ValidationError(strWarning)
        result = results[0]
        return result

    
class TaxYear(models.Model):
    _name = 'tax.year'
    _description = "Tax Year"
    _order = "date_start asc, id"


    name = fields.Char('Tax Year', required=True)
    code = fields.Char('Code', required=True)
    date_start = fields.Date('Date Start', required=True)
    date_end = fields.Date('Date End', required=True)
    period_ids = fields.One2many('tax.period', 'year_id', 'Periods')

    @api.constrains("date_start", "date_end")
    def _check_date_range(self):
        if self.date_end <= self.date_start:
            strWarning = _("The start date must precede it's end date")
            raise ValidationError(strWarning)

    @api.multi
    def action_create_period(self):
        for year in self:
            year._create_period()

    @api.multi
    def _create_period(self):
        self.ensure_one()
        obj_period = self.env["tax.period"]
        date_start = datetime.strptime(self.date_start, "%Y-%m-%d")
        while date_start.strftime("%Y-%m-%d") < self.date_end:
            date_end = date_start + relativedelta(months=+1, days=-1)

            if date_end.strftime("%Y-%m-%d") > self.date_end:
                date_end = datetime.strptime(self.date_end, "%Y-%m-%d")

            obj_period.create({
                "name": date_start.strftime("%m/%Y"),
                "code": date_start.strftime("%m/%Y"),
                "date_start": date_start.strftime("%Y-%m-%d"),
                "date_end": date_end.strftime("%Y-%m-%d"),
                "year_id": self.id,
            })
            date_start = date_start + relativedelta(months=+1)
