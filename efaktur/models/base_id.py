# -*- coding: utf-8 -*-

from odoo import models, fields, api


class CountryState(models.Model):
    _inherit = "res.country.state"
    
    name = fields.Char(string='Province')
    kabupaten_line = fields.One2many('res.kabupaten', 'state_id', string='Kabupaten')


class Kabupaten(models.Model):
    _name = "res.kabupaten"
    _description = "List Kabupaten"
    
    name = fields.Char(string='Kabupaten')
    state_id = fields.Many2one('res.country.state', string="Province")
    kecamatan_line = fields.One2many('res.kecamatan', 'kabupaten_id', string='Kecamatan')
    

class Kecamatan(models.Model):
    _name = "res.kecamatan"
    _description = "List Kecamatan"
    
    name = fields.Char(string='Kecamatan')
    state_id = fields.Many2one('res.country.state', string="Province")
    kabupaten_id = fields.Many2one('res.kabupaten', string="Kabupaten")
    kelurahan_line = fields.One2many('res.kelurahan', 'kecamatan_id', string='Kelurahan')


class Kelurahan(models.Model):
    _name = "res.kelurahan"
    _description = "List Kelurahan"
    
    name = fields.Char(string='Kelurahan')
    state_id = fields.Many2one('res.country.state', string="Province")
    kabupaten_id = fields.Many2one('res.kabupaten', string="Kabupaten")
    kecamatan_id = fields.Many2one('res.kecamatan', string="Kecamatan")
    zip = fields.Char("Kode Pos")
