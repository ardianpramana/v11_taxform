# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import ValidationError
import openerp.addons.decimal_precision as dp


class FakturPajak(models.Model):
	_name = "nomor.faktur.pajak"
	_description = 'Nomor faktur Pajak'
	_sql_constraints = [
			('faktur_unique', 'unique(nomor_perusahaan,tahun_penerbit,nomor_urut)', 'Number Faktur Must Be Unique.'),
		]

	def get_faktur_type(self):
		return self._context.get('type')

	name = fields.Char(string='Nomor Faktur', compute='_get_nomor_faktur', store=True)
	nomor_perusahaan = fields.Char('Nomor Perusahaan', size=3)
	tahun_penerbit = fields.Char('Tahun Penerbit', size=2)
	nomor_urut = fields.Char('Nomor Urut', size=8)
	number = fields.Char(string='Nomor eFaktur', compute='_get_nomor_faktur', store=True)
	invoice_id = fields.Many2one('account.invoice', 'Invoice No')
	partner_id = fields.Many2one('res.partner', string='Partner', related='invoice_id.partner_id', store=True)
	amount_untaxed = fields.Monetary(string='Untaxed Amount', related='invoice_id.amount_untaxed', 
		digits=dp.get_precision('Account'), store=True)
	amount_tax = fields.Monetary(string='Tax Amount', related='invoice_id.amount_tax', 
		digits=dp.get_precision('Account'), store=True)
	date_invoice = fields.Date(string='Date Used', related='invoice_id.date_invoice', store=True)
	company_id = fields.Many2one('res.company', string='Company', related='invoice_id.company_id', store=True)
	currency_id = fields.Many2one('res.currency', string='Currency', related='invoice_id.currency_id', store=True)
	type = fields.Selection([
			('in','Faktur Pajak Masukan'),
			('out','Faktur Pajak Keluaran')
		], string='Type', default=get_faktur_type)
	state = fields.Selection([
			('1','Used'),
			('0','Not Used')
		],string='Status', compute='_get_nomor_faktur', store=True)

	@api.multi
	@api.depends('nomor_perusahaan', 'tahun_penerbit', 'nomor_urut', 'invoice_id', 'invoice_id.state')
	def _get_nomor_faktur(self):
		for faktur in self:
			faktur.name = "%s.%s.%s" % (faktur.nomor_perusahaan, faktur.tahun_penerbit, faktur.nomor_urut)
			faktur.number = "%s%s%s" % (faktur.nomor_perusahaan, faktur.tahun_penerbit, faktur.nomor_urut)
			faktur.state = '0'
			if faktur.invoice_id and faktur.invoice_id.state not in ('draft','cancel'):
				faktur.state = '1'
