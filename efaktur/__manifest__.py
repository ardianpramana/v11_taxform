{
    "name"          : "E-Faktur",
    "version"       : "11.0",
    'license'       : 'AGPL-3',
    'images'        : ['static/description/main_screenshot.jpg'],
    "author"        : "Alphasoft,"
                      "Arkana Solusi Digital",
    "description"   : """This module aim to:
                    - Create Object Nomor Faktur Pajak
                    - Add Column Customer such as: 
                        * NPWP, RT, RW, Kelurahan, Kecamatan, Kabupaten, Province
                    - Just Import the file csv at directory data
                    - Export file csv for upload to efaktur""",
    "website"       : "https://www.alphasoft.co.id/",
    "category"      : "Accounting",
    'price'         : 189.00,
    'currency'      : 'EUR',
    "init_xml"      : [],
    "demo_xml"      : [],
    'test'          : [],    
    "active"        : False,
    "installable"   : True,
    'live_test_url': '',

    # any module necessary for this one to work correctly
    'depends': ['account','contacts'],
    'external_dependencies': {'python': ['xlwt']},

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'data/res_country_data.xml',
        'views/base_views.xml',
        'views/res_partner_views.xml',
        'views/faktur_pajak_views.xml',
        'views/account_invoice_views.xml',
        'wizard/faktur_pajak_generate_views.xml',
    ],
}