# -*- coding: utf-8 -*-
# Copyright 2016 OpenSynergy Indonesia
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).
# Modified by Arkana Solusi Digital 2018
{
    'name': "Indonesia's Taxform - PPh 21 Computation On Payslip",
    'version': '11.0',
    'category': 'localization',
    'website': 'https://www.arkana.co.id/',
    'author': 'OpenSynergy Indonesia,'
              'Odoo Community Association (OCA),'
              'Arkana Solusi Digital,'
              'Joenan,'
              'Ardian',
    'license': 'AGPL-3',
    'installable': True,
    'depends': [
        'id_taxform_pph_21',
        'id_taxform_employee_joining_period',
        'hr_payroll',
    ],
    'data': [
        'views/hr_payslip_views.xml',
        'data/hr_salary_rule_category_data.xml',
        'data/hr_salary_rule_data.xml',
    ],
}