# -*- coding: utf-8 -*-
# Copyright 2016 OpenSynergy Indonesia
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).
# Modified by Arkana Solusi Digital 2018
{
    "name": "Indonesia - Employee's Joining Time Based on Tax Period",
    "version": "11.0.0.0.0",
    "category": "localization",
    "website": "https://www.arkana.co.id/",
    "author": "OpenSynergy Indonesia,"
              "Odoo Community Association (OCA),"
              "Arkana Solusi Digital,"
              "Joenan,"
              "Ardian",
    "license": "AGPL-3",
    "installable": True,
    'depends': [
        'id_taxform_period',
        'hr_contract'
    ],
    'data': [
        'views/hr_employee_views.xml',
    ],
}